package com.orderdemo;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.orderdemo.controllers.OrderController;
import com.orderdemo.controllers.UserController;
import com.orderdemo.dto.Orders;
import com.orderdemo.dto.User;
import com.orderdemo.repos.OrderRepository;
import com.orderdemo.repos.UserRepository;

@ExtendWith(MockitoExtension.class)
public class OrderControllerTest {

	@Mock
	OrderRepository repo;

	@InjectMocks
	OrderController controller;

	@Mock
	UserRepository userRepo;

	@InjectMocks
	UserController userController;

	@Test
	public void getAllOrders() {
		List<Orders> order = new ArrayList<Orders>();
		Orders orders = new Orders();
		orders.setPhone("9966650120");
		order.add(orders);
		when(repo.findAll()).thenReturn(order);
		List<Orders> orders2 = controller.getAllOrders();
		assertThat(orders2.get(0).getPhone()).isEqualTo("9966650120");
	}

	@Test
	public void deleteOrder() throws Exception {
		doNothing().when(repo).deleteById(1L);
		controller.deleteOrderById(1L);
	}

	@Test
	public void saveOrderTest() {

		Orders order = new Orders();
		order.setCustomerName("jyoti");
		order.setItem("book");
		order.setPhone("7993912348");
		order.setFromAddress("East Godawari");
		order.setToAddress("vizag");

		when(repo.save(order)).thenReturn(order);
		assertEquals(order, controller.insertOrder(order, 3L));

	}

	@Test
	public void saveOrdersTest() {
		List<Orders> orders = new ArrayList<Orders>();

		Orders order0 = new Orders();
		order0.setId(7L);
		order0.setCustomerName("jyoti");
		order0.setItem("book");
		order0.setPhone("7993912348");
		order0.setFromAddress("East Godawari");
		order0.setToAddress("vizag");
		orders.add(order0);

		Orders order1 = new Orders();
		order1.setId(15L);
		order1.setCustomerName("raju");
		order1.setItem("laptop");
		order1.setPhone("9966650120");
		order1.setFromAddress("East Godawari");
		order1.setToAddress("vizag");
		orders.add(order1);

		

		when(repo.saveAll(orders)).thenReturn(orders);
		assertEquals(orders, controller.insertMultipleStudents(orders, 3L));
		//assertThat(orders.get(0).getCustomerName()).isEqualTo(orders2.get(0).getCustomerName());

	}

	

	@Test 
	public void saveUserTest() {

		User user = new User();

		user.setName("mani");
		user.setEmail("mani@gmail.com");
		user.setPassword("1234");

		when(userRepo.save(user)).thenReturn(user);
		assertEquals(user, userController.createUser(user));

	}
	
	
	
	@Test
	public void deleteUserTest() {
		
		doNothing().when(userRepo).deleteById(1L);
		userController.deleteUserById(1L);
	}
	

	@Test
	public void getUserTest() {
		
		User user = new User();
		when(userRepo.getOne(1L)).thenReturn(user);
		
		
		assertEquals(user, userController.getUserById(1L));
	
	}
	
	@Test
	public void getOrder() {

		Orders order = new Orders();
		order.setId(1L);
		order.setCustomerName("raju");
		when(repo.getOne(1L)).thenReturn(order);
		Orders orders2 = controller.getOrderById(1L);
		assertEquals(order.getCustomerName(),orders2.getCustomerName());

	}

	
	
	@Test
	public void putUserTest() {
		
		User user = new User();

		user.setName("mani kanta");
		user.setEmail("mani@gmail.com");
		user.setPassword("1234");

		when(userRepo.save(user)).thenReturn(user);
		//assertEquals(user, userController.updateUserById(user,2L));

		
	}
	
	@Test
	public void getOrderExceptionTest() {
		
		Orders order = new Orders();
		
		doThrow(new NullPointerException()).when(repo).save(order);
		 assertThat(controller.insertOrder(order, 1L));	
		
	}
	
	
	
}
