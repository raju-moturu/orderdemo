package com.orderdemo.dto;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.SQLDelete;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@SQLDelete(sql = "UPDATE orders SET delete_date = SYSDATE() WHERE id = ?")
public class Orders {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String customerName;
	private String phone;
	private String item;
	private String fromAddress;
	private String toAddress;
	private Date deleteDate;

	
	@Temporal(TemporalType.TIMESTAMP)
	private Date createDate;
	
	@PrePersist
	private void onCreate() {
		createDate=new Date();
	}
	
	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	
	@JsonIgnore
	@ManyToOne
	@JoinColumn(name = "user_id")
	private User user;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getItem() {
		return item;
	}

	public void setItem(String item) {
		this.item = item;
	}

	public String getFromAddress() {
		return fromAddress;
	}

	public void setFromAddress(String fromAddress) {
		this.fromAddress = fromAddress;
	}

	public String getToAddress() {
		return toAddress;
	}

	public void setToAddress(String toAddress) {
		this.toAddress = toAddress;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public Date getDeleteDate() {
		return deleteDate;
	}

	public void setDeleteDate(Date deleteDate) {
		this.deleteDate = deleteDate;
	}

	public User getUser() {
		return user;
	}

	

	public Orders() {
		super();
	}

	public Orders(String customerName, String phone, String item, String fromAddress, String toAddress) {
		super();
		this.customerName = customerName;
		this.phone = phone;
		this.item = item;
		this.fromAddress = fromAddress;
		this.toAddress = toAddress;
	}

	

	public void setUser(User user) {
		this.user = user;
	}

	@Override
	public String toString() {
		return "Orders [id=" + id + ", customerName=" + customerName + ", phone=" + phone + ", item=" + item
				+ ", fromAddress=" + fromAddress + ", tgoAddress=" + toAddress + ", createDate=" + createDate
				+ ", deleteDate=" + deleteDate + ", user=" + user + "]";
	}

	

}
