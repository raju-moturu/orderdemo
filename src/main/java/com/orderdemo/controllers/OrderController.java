package com.orderdemo.controllers;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.orderdemo.dto.Orders;
import com.orderdemo.dto.User;
import com.orderdemo.exceptions.ResourceNotFoundExcption;
import com.orderdemo.pojo.OrderPojo;
import com.orderdemo.repos.OrderRepository;
import com.orderdemo.repos.UserRepository;


//order controller
@RestController
@RequestMapping("/orders")
public class OrderController {

	@Autowired
	private OrderRepository orderRepo;

	@Autowired
	private UserRepository userRepo;

	@PostMapping("/{id}")
	public Orders insertOrder(@RequestBody Orders order, @PathVariable("id") Long id) {
		User user = userRepo.getOne(id);
		order.setUser(user);
		return orderRepo.save(order);
	}

	@PostMapping("/orders/{id}")
	public List<Orders> insertMultipleStudents(@RequestBody List<Orders> orders, @PathVariable("id") Long id) {

		try {
			User user = userRepo.getOne(id);

			for (Orders orders2 : orders) {
				orders2.setUser(user);
			}

		} catch (NullPointerException e) {
			e.printStackTrace();
		}
		return orderRepo.saveAll(orders);
	}

	@GetMapping
	public List<Orders> getAllOrders() {
		return orderRepo.findAll();
	}

	@GetMapping("/{id}")
	public Orders getOrderById(@PathVariable("id") Long id) {
		return orderRepo.findById(id).orElseThrow(() -> new ResourceNotFoundExcption("order not found"));

	}

	

	@DeleteMapping("orders/{id}")
	public void deleteById(@PathVariable long id, @RequestBody OrderPojo orderpojo) throws Exception {

		// User user=userRepo.findById(id).get();

		for (Long orderid : orderpojo.getOrderid()) {
			Orders order = orderRepo.findById(orderid).get();
			if (order.getUser().getId() == id) {
				String d = java.time.LocalDate.now().toString();
				Date date = new SimpleDateFormat("yyyy-MM-dd").parse(d);

				order.setDeleteDate(date);
				orderRepo.save(order);
			}

		}

	}

	@PutMapping("{id}")
	public Orders updateOrderByUserId(@RequestBody Orders orders, @PathVariable long id) {

		try {
			Orders orders2 = orderRepo.getOne(id);

			User user = userRepo.getOne(orders2.getUser().getId());

			orders.setUser(user);
			orders.setId(id);

		} catch (Exception e) {

		}
		return orderRepo.save(orders);

	}

	@DeleteMapping("orderdelte/{id}")
	public void deleteOrderById(@PathVariable("id") long id) {

		orderRepo.deleteById(id);
	}

	@GetMapping("/phone/{phone}")
	public List<Orders> getOrdersByPhone(@PathVariable("phone") String phone) {
		return orderRepo.findByPhone(phone);
	}

	@GetMapping("/deleted")
	public List<Orders> getDeletedOrders() {
		return orderRepo.findDeletedOrders();
	}

	@GetMapping("/active/{id}")
	public List<Orders> getActiveOrdersByUserId(@PathVariable("id") Long id) {

		return orderRepo.findActiveOrderByUderId(id);

	}

	@GetMapping("/delete/{id}")
	public List<Orders> getDeletedOrdersByUserId(@PathVariable("id") Long id) {
		return orderRepo.findDeletedOrdersByUserId(id);

	}
	
	@DeleteMapping("/{id}")
	public void deleteOrderId(@PathVariable("id") Long id) {
		orderRepo.deleteById(id);
	}

}