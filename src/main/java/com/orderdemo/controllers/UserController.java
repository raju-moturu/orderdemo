package com.orderdemo.controllers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.orderdemo.dto.User;
import com.orderdemo.exceptions.ResourceNotFoundExcption;
import com.orderdemo.exceptions.ServerException;
import com.orderdemo.repos.UserRepository;

@RestController
@RequestMapping("/user")
public class UserController {

	@Autowired
	private UserRepository userRepo;

	@PostMapping
	public User createUser(@RequestBody User user) {

		User user1 = userRepo.findByEmail(user.getEmail());
		if (user1 == null)
			return userRepo.save(user);
		else {
			new ResourceNotFoundExcption("Record already existed");
			return null;
		}

	}
	

	@PostMapping("/users")
	public List<User> createUsers(@RequestBody List<User> users) {

		List<User> addUsers = new ArrayList<User>();

		for (User user : users) {
			User user1 = userRepo.findByEmail(user.getEmail());
			if (user1 == null) {
				addUsers.add(user);
			}

		}

		return userRepo.saveAll(addUsers);
	}
	
	

	@GetMapping("/{id}")
	public User getUserById(@PathVariable("id") Long id) {

		return userRepo.findById(id).orElseThrow(() -> new ServerException("No cutomer for id:" + id));
	}
	
	

	
	/*
	 * @PutMapping("/{id}") public ResponseEntity<Object>
	 * updateUserById(@RequestBody User user, @PathVariable("id") Long id) {
	 * 
	 * User user1 = userRepo.findById(id).get(); if (user1 == null) { return
	 * ResponseEntity.notFound().build(); }
	 * 
	 * User user2 = userRepo.findByEmail(user.getEmail()); if (user2==null ||
	 * user1.getEmail().equalsIgnoreCase(user.getEmail())) { user.setId(id);
	 * userRepo.save(user); return ResponseEntity.ok(user); } return null;
	 * 
	 * }
	 */
	 
	
	@DeleteMapping("{id}")
	public void deleteUserById(@PathVariable("id") Long id) {
		userRepo.deleteById(id);
	}
	
	@DeleteMapping("/delete/{id}")
	public void deleteUserByUserId(@PathVariable("id") Long id) {
		userRepo.deleteById(id);
	}
	
	

}
