package com.orderdemo.repos;

import org.springframework.data.jpa.repository.JpaRepository;

import com.orderdemo.dto.User;

public interface UserRepository extends JpaRepository<User, Long> {

	User findByEmail(String email);
	
	

}
