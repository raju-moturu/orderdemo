package com.orderdemo.repos;

import org.springframework.data.jpa.repository.JpaRepository;

import com.orderdemo.dto.Student;

public interface StudentRepo extends JpaRepository<Student, Integer> {

}
