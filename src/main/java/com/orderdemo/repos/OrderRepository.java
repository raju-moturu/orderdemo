package com.orderdemo.repos;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.orderdemo.dto.Orders;


public interface OrderRepository extends JpaRepository<Orders, Long>,JpaSpecificationExecutor<Orders> {

	List<Orders> findByPhone(String phone);

	@Query(value="SELECT * FROM Orders WHERE delete_date is null",nativeQuery = true)
	List<Orders> findDeletedOrders();

	@Query(value="SELECT * FROM Orders WHERE user_id =:userId AND delete_date is not null ",nativeQuery = true)
	List<Orders> findActiveOrderByUderId(@Param("userId") Long id);

	@Query(value="SELECT * FROM Orders WHERE user_id =:userId AND delete_date is null ",nativeQuery = true)
	List<Orders> findDeletedOrdersByUserId(@Param("userId") Long id);
	

}
