package com.orderdemo.pojo;

import java.util.Set;

import com.fasterxml.jackson.annotation.JsonProperty;

public class OrderPojo {

	@JsonProperty("orderid")
	private Set<Long> orderid;

	public Set<Long> getOrderid() {
		return orderid;
	}

	public void setOrderid(Set<Long> orderid) {
		this.orderid = orderid;
	}

	@Override
	public String toString() {
		return "OrderPojo [orderid=" + orderid + "]";
	}

	
	

	
	

}
