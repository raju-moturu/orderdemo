package com.orderdemo.exceptions;

public class ResourceNotFoundExcption extends RuntimeException {

	private static final long serialVersionUID = 1L;
	
	public ResourceNotFoundExcption(String message) {
		super(message);
	}

}
