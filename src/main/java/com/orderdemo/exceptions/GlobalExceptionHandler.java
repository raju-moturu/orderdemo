package com.orderdemo.exceptions;

import java.util.Date;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

@ControllerAdvice
public class GlobalExceptionHandler {
	
	@ExceptionHandler(ResourceNotFoundExcption.class)
	public ResponseEntity<?> handleResourceNotFound(ResourceNotFoundExcption exception,WebRequest request){
		ErrorDetails errorDetails=new ErrorDetails(new Date(), exception.getMessage(), request.getDescription(false));
		return new ResponseEntity<Object>(errorDetails,HttpStatus.NOT_FOUND);
		
	}
	
	@ExceptionHandler(ServerException.class)
	public ResponseEntity<?> handleInternalServer(ServerException exception,WebRequest request){
		ErrorDetails errorDetails=new ErrorDetails(new Date(), exception.getMessage(), request.getDescription(false));
		return new ResponseEntity<Object>(errorDetails,HttpStatus.INTERNAL_SERVER_ERROR);
		
	}
	
	
 
}
